class DataSizeError(Exception):
    pass


class WrongIndexError(Exception):
    pass


class MultOperandError(Exception):
    pass


class Matrix:
    def __init__(self, size, data=None):
        if len(size) == 2:
            self.__size = size
        else:
            raise DataSizeError
        if data is None:
            self.__data = [[0 for _ in range(size[1])] for _ in range(size[0])]
        elif len(data) == size[0] and all(map(lambda x: len(x) == size[1], data)):
            self.__data = data
        else:
            raise DataSizeError

    def __str__(self):
        out = ''
        for row in self.__data:
            for element in row:
                out += str(element) + ' '
            out = out.strip(' ')
            out += '\n'
        out = out.strip('\n')
        return out

    def __repr__(self):
        return "Matrix({0}, {1})".format(self.__size, self.__data)

    def __eq__(self, other):
        return self.size == other.size and self.data == other.data

    def __getitem__(self, coord):
        """
        Переопределение стандартного метода для поддержки [].

        Позволяет получать элемент матрицы, используя инструкции вида
        "matrix[(row, column)]". row и column должны быть целыми числами
        в диапазоне от 0 до self.__size - 1, иначе будет возбуждено
        исключение WrongIndexError. Если параметр coord содержит отличное
        от 2 число элементов, также возбуждается исключение WrongIndexError.
        """

        if len(coord) == 2 and coord[0] in range(self.__size[0]) and coord[1] in \
                range(self.__size[1]):
            return self.__data[coord[0]][coord[1]]
        else:
            raise WrongIndexError

    def __setitem__(self, coord, value):
        if len(coord) == 2 and coord[0] in range(self.__size[0]) and coord[1] in \
                range(self.__size[1]):
            self.__data[coord[0]][coord[1]] = value
        else:
            raise WrongIndexError

    def __mul__(self, other):
        if self.size[1] == other.size[0]:
            size = (self.size[0], other.size[1])
            if size[0] == 1 and size[1] == 1:
                return sum(map(lambda x: x[0] * x[1], zip(self.get_row(0),
                                                          other.get_column(0))))
            elif size[0] == 1:
                data = [sum(map(lambda x: x[0] * x[1], zip(self.get_row(0),
                                                           other.get_column(i))))
                        for i in range(size[1])]
                return Row(size[1], data)
            elif size[1] == 1:
                data = [sum(map(lambda x: x[0] * x[1], zip(self.get_row(i),
                                                           other.get_column(0))))
                        for i in range(size[0])]
                return Column(size[0], data)
            else:
                data = [[sum(map(lambda x: x[0] * x[1], zip(self.get_row(i),
                                                            other.get_column(j))))
                         for j in range(size[1])] for i in range(size[0])]
                return Matrix(size, data)
        else:
            raise MultOperandError("Wrong operand size: {0} and {1}".format(self.size,
                                                                            other.size))

    @property
    def size(self):
        return self.__size

    @property
    def data(self):
        return self.__data


    def get_row(self, index):
        """
        Получить строку по ее индексу, начиная с 0.

        Если индекс лежит в пределах от 0 до self._size[0] - 1 и является
        целым числом метод возвращает строку, иначе возбуждается исключение
        WrongIndexError
        """

        if index in range(self.__size[0]):
            return self.__data[index]
        else:
            raise WrongIndexError

    def get_column(self, index):
        """
        Полностью аналогичен методу swap_rows, но работает со столбцами
        """

        if index in range(self.__size[1]):
            column = []
            for row in self.__data:
                column.append(row[index])
            return column
        else:
            raise WrongIndexError

    def swap_rows(self, first, second):
        """
        Поменять местами строки с указанными индексами, начиная с 0.

        Ограничения на индексы совпадают с ограничениями в методе get_row.
        """

        first_row = self.get_row(first)
        second_row = self.get_row(second)

        self.__data[first] = second_row
        self.__data[second] = first_row

    def swap_columns(self, first, second):
        """
        Полностью аналогичен методу swap_rows, но работает со столбцами
        """

        first_column = self.get_column(first)
        second_column = self.get_column(second)

        for i in range(self.__size[0]):
            self.__data[i][first] = second_column.pop(0)
            self.__data[i][second] = first_column.pop(0)

    def row_minus_row(self, mri, sri):
        """
        Вычитает одну строку из другой.

        Этот метод является атомарным шагом прямого прохода метода
        Гаусса. Вычитаемая строка домножается на коэффициент,
        подобранный так, чтобы элемент второй строки, находящийся под(над)
        элементом главной строки на главной диагонали, стал равным нулю,
        подобранный коэффициент возвращается. mri - главная (вычитаемая
        строка), sri - уменьшаемая строка
        """

        main_row = self.get_row(mri)
        second_row = self.get_row(sri)
        coef = 0 if second_row[mri] == 0 else second_row[mri] / main_row[mri]
        self.__data[sri] = list(map(lambda x: x[0] - x[1], zip(second_row, list(map(
            lambda x: x * coef, main_row)))))
        return coef

    @classmethod
    def not_null(cls, row):
        return not all(map(lambda x: x == 0, row))

    @property
    def rank(self):
        """
        Возвращает ранг матрицы.

        Так как это свойство вызывается после выполнения прямого хода
        метода Гаусса, предполагается, что матрица приведена к треугольной
        (трапициевидной) и ее ранг в этом случае равен количеству ненулевых
        строк.
        """
        return len(list(filter(Matrix.not_null, self.__data)))

    def div_row(self, index):
        divisor = self[(index, index)]
        for i in range(self.size[1]):
            self[(index, i)] /= divisor


class Column(Matrix):
    def __init__(self, size, data=None):
        if data is None:
            super().__init__((size, 1))
        else:
            super().__init__((size, 1), [[x] for x in data])

    def __repr__(self):
        return "Column({0}, {1})".format(self.size[0], [x[0] for x in self.data])

    def __getitem__(self, item):
        return super().__getitem__((item, 0))

    def __setitem__(self, key, value):
        super().__setitem__((key, 0), value)

    def swap(self, first, second):
        super().swap_rows(first, second)

    def row_minus_row(self, mi, si, coefficient):
        main = self[mi]
        second = self[si]

        self[si] = second - coefficient * main

    def div(self, index, divisor):
        self[index] /= divisor


class Row(Matrix):
    def __init__(self, size, data=None):
        if data is None:
            super().__init__((1, size))
        else:
            super().__init__((1, size), [data])

    def __repr__(self):
        return "Row({0}, {1})".format(self.size[1], self.data[0])

    def __getitem__(self, item):
        return super().__getitem__((0, item))

    def __setitem__(self, key, value):
        super().__setitem__((0, key), value)

    def swap(self, first, second):
        super().swap_columns(first, second)
