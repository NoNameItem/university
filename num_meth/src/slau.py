from matrices import Matrix, Column


class NotEqualSizeError(Exception):
    pass


class NotSquareError(Exception):
    pass


class Slau:
    def __init__(self, matrix, free_column, variables=None):
        """
        Конструктор СЛАУ.

        Если число строк матрица, переданной в первом параметре, не
        совпадет с числом элементов стоблца свободных переменных,
        переданных во втором параметре, возбуждается исключение
        NotEqualSizeError.
        """
        if matrix.size[0] == free_column.size[0] and matrix.size[1] == len(variables):
            self.__square = matrix.size[0] == matrix.size[1]
            self.__matrix = matrix
            self.__free_column = free_column
            self.__eq_num = matrix.size[0]
            self.__var_num = matrix.size[1]
            self.__variables = ["x{0}".format(x) for x in range(1,
                                                                self.__var_num + 1)]
        else:
            raise NotEqualSizeError

    def __str__(self):
        out = ""
        for i in range(self.__eq_num):
            line = ""
            for j in range(self.__var_num):
                line += " + " if self.__matrix[i, j] >= 0 else " - "
                line += str(abs(self.__matrix[i, j])) if abs(self.__matrix[i, j]) != 1 \
                    else ''
                line += self.__variables[j]
            if line.startswith(" - "):
                line = '-' + line[3::]
            else:
                line = line[3::]
            out += line
            out += " = " + str(self.__free_column[i]) + '\n'
        out = out.strip('\n')
        return out

    def __repr__(self):
        column_data = [x[0] for x in self.__free_column.data]
        return "Slau(Matrix(({0.equations_number}, {0.variables_number}), " \
               "{0.matrix.data}), Column({0.equations_number},{1}))".format(self,
                                                                            column_data)

    def __eq__(self, other):
        return self.matrix == other.matrix and self.free_column == other.free_column and \
            self.variables == other.variables

    @property
    def matrix(self):
        return self.__matrix

    @property
    def free_column(self):
        return self.__free_column

    @property
    def variables(self):
        return self.__variables

    @property
    def equations_number(self):
        return self.__eq_num

    @property
    def variables_number(self):
        return self.__var_num

    @property
    def extended_matrix(self):
        new_data = []
        for i in range(self.equations_number):
            new_data.append(self.matrix.get_row(i) + [self.free_column[i]])
        return Matrix((self.matrix.size[0], self.matrix.size[1] + 1), new_data)

    def _swap_rows(self, first, second):
        self.__matrix.swap_rows(first, second)
        self.__free_column.swap(first, second)

    def _swap_columns(self, first, second):
        self.__matrix.swap_columns(first, second)
        self.__variables[first], self.__variables[second] = self.__variables[second], \
            self.__variables[first]

    def __row_minus_row(self, mri, sri):
        self.__free_column.row_minus_row(mri, sri, self.__matrix.row_minus_row(mri, sri))

    def div_eq(self, index):
        self.__free_column.div(index, self.__matrix.div_row(index))

    def __find_not_zero(self, start):
        for i in range(start, self.equations_number):
            if self.matrix[start - 1, i] != 0:
                return i
        return None

    def _go_down_normal(self):
        """
        Прямой ход метода Гаусса.

        Матрица системы приводится к треугольной (в общем случае
        трапециевидной). Со столбцом свободных чисел происходят изменения,
        аналогичные изменениям, производящимся над матрицей системы.
        Если вдруг на каком-либо шаге основной элемент оказывается нулевым,
        происходит перестановка уравнений так, чтобы основной элемент
        был отличен от нуля, если такой элемент не может быть найден,
        считается, что этот шаг уже выполнен и происходит переход к
        следующей итерации.
        """
        i = 0
        while i < self.equations_number:
            if self.matrix[i, i] == 0:
                not_zero_index = self.__find_not_zero(i + 1)
                if not_zero_index:
                    self._swap_rows(i, not_zero_index)
                else:
                    continue
            j = i + 1
            while j < self.equations_number:
                self.__row_minus_row(i, j)
                j += 1
            i += 1

    def up(self):
        """
        Обратный ход метода Гаусса.

        Выполняется в предположении, что система квадратная.
        """
        if not self.__square:
            raise NotSquareError
        i = self.equations_number - 1
        while i > 0:
            self.div_eq(i)
            j = i - 1
            while j >= 0:
                self.__row_minus_row(i, j)
                j -= 1
            i -= 1

    def gauss_method(self):
        self._go_down_normal()
        self._delete_null_eqs()

        if self.matrix.rank != self.extended_matrix.rank:   # Теорема Кроннекера-Капелли
            return []

        main_var_num = self.matrix.rank
        free_var_num = self.__var_num - main_var_num
        square_slau = self._reduce_to_square(main_var_num)
        fsr = square_slau.up()

        add = []
        for i in range(free_var_num):
            tmp = [0 for _ in range(free_var_num)]
            tmp[i] = 1
            add.append(tmp)

        if not add:
            return [fsr]
        else:
            return list(map(lambda x: fsr + x, add))

    def _reduce_to_square(self, size):
        column_data = self.__free_column.data[:size]
        matrix_data = []
        for i in range(size):
            matrix_data.append(self.__matrix.get_row(i)[:size])
        return Slau(Matrix((size, size), matrix_data), Column(size, column_data))

    def _delete_null_eqs(self):
        matrix_data = []
        column_data = []

        for i in range(self.equations_number):
            if Matrix.not_null(self.__matrix.get_row(i)) and self.__free_column[i] != 0:
                matrix_data.append(self.__matrix.get_row(i))
                column_data.append(self.__free_column[i])

        self.__matrix = Matrix((len(matrix_data), self.__var_num), matrix_data)
        self.__free_column = Column(len(matrix_data), column_data)
        self.__eq_num = len(matrix_data)