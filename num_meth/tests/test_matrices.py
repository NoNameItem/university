#!/usr/bin/env python3

import unittest
from src.matrices import Matrix
from src.matrices import Column
from src.matrices import Row
from src.matrices import DataSizeError
from src.matrices import WrongIndexError


class MatrixTestCase(unittest.TestCase):
    def test_wrong_size1(self):
        self.assertRaises(DataSizeError, Matrix, (5, 5), [[]])

    def test_wrong_size2(self):
        self.assertRaises(DataSizeError, Matrix, (3, 1), [[2, 1], [3]])

    def test_str(self):
        m = Matrix((2, 3))
        self.assertEqual(str(m), "0 0 0\n0 0 0")

    def test_repr(self):
        m = Matrix((5, 3))
        self.assertEqual(m, eval(m.__repr__()))

    def test_equal(self):
        m = Matrix((2, 2), [[2, 4], [2, 4]])
        self.assertTrue(m == m)

    def test_non_equal1(self):
        m1 = Matrix((2, 2), [[4, 7], [50, 54]])
        m2 = Matrix((2, 2), [[4, 8], [50, 54]])
        self.assertFalse(m1 == m2)

    def test_non_equal2(self):
        m1 = Matrix((2, 3))
        m2 = Matrix((5, 7))
        self.assertFalse(m1 == m2)

    def test_float_row_index(self):
        m = Matrix((3, 3), [[1, 2, 3], [3, 4, 5], [6, 7, 9]])
        self.assertRaises(WrongIndexError, m.get_row, 2.5)

    def test_neg_row_index(self):
        m = Matrix((1, 10))
        self.assertRaises(WrongIndexError, m.get_row, -10)

    def test_big_row_index(self):
        m = Matrix((2, 5))
        self.assertRaises(WrongIndexError, m.get_row, 10000000)

    def test_get_row(self):
        m = Matrix((3, 3), [[5, 10, -2], [78, 54, 6], [0, 5, -87]])
        self.assertEqual(m.get_row(1), [78, 54, 6])

    def test_float_column_index(self):
        m = Matrix((3, 3), [[1, 2, 3], [3, 4, 5], [6, 7, 9]])
        self.assertRaises(WrongIndexError, m.get_column, 2.5)

    def test_neg_column_index(self):
        m = Matrix((2, 10))
        self.assertRaises(WrongIndexError, m.get_row, -10)

    def test_big_column_index(self):
        m = Matrix((5, 4))
        self.assertRaises(WrongIndexError, m.get_row, 10000000)

    def test_get_column(self):
        m = Matrix((4, 4), [[55, 60, 10, -2], [3, 78, 54, 6], [0, 5, 540, -87], [87, 96, -150, 2]])
        self.assertEqual(m.get_column(1), [60, 78, 5, 96])

    def test_swap_rows(self):
        m = Matrix((3, 3), [[1, 2, 3], [5, 7, 9], [4, 4, 7]])
        m.swap_rows(1, 2)
        m_new = Matrix((3, 3), [[1, 2, 3], [4, 4, 7], [5, 7, 9]])
        self.assertEqual(m, m_new)

    def test_swap_columns(self):
        m = Matrix((4, 4), [[1, 2, 3, 4], [5, 6, 7, 8], [9, 0, 1, 2], [3, 4, 5, 6]])
        m.swap_columns(0, 3)
        m_new = Matrix((4, 4), [[4, 2, 3, 1], [8, 6, 7, 5], [2, 0, 1, 9], [6, 4, 5, 3]])
        self.assertEqual(m, m_new)

    def test_get_elem(self):
        m = Matrix((3, 3), [[1, 2, 3], [-4, -5, -6], [7, -8, 9]])
        self.assertEqual(m[(1, 0)], -4)

    def test_set_elem(self):
        m = Matrix((2, 2))
        m[(0, 1)] = 1
        m_new = Matrix((2, 2), [[0, 1], [0, 0]])
        self.assertEqual(m, m_new)

    def test_short_index(self):
        m = Matrix((4,2))
        self.assertRaises(WrongIndexError, m.__getitem__, [])

    def test_row_minus_row(self):
        m = Matrix((2, 2), [[1, 1], [2, 3]])
        m.row_minus_row(0, 1)
        m_new = Matrix((2, 2), [[1, 1], [0, 1]])
        self.assertEqual(m, m_new)


class ColumnTestCase(unittest.TestCase):
    def test_wrong_size1(self):
        self.assertRaises(DataSizeError, Column, 3, [])

    def test_wrong_size2(self):
        self.assertRaises(DataSizeError, Column, 3, [2, 1])

    def test_str(self):
        c = Column(3)
        self.assertEqual(str(c), "0\n0\n0")

    def test_repr(self):
        c = Column(3)
        self.assertEqual(c, eval(c.__repr__()))

    def test_equal(self):
        c = Column(2, [2, 4])
        self.assertTrue(c == c)

    def test_non_equal1(self):
        c1 = Column(2, [4, 7])
        c2 = Column(2, [4, 8])
        self.assertFalse(c1 == c2)

    def test_non_equal2(self):
        c1 = Column(2)
        c2 = Column(5)
        self.assertFalse(c1 == c2)

    def test_swap(self):
        c = Column(3, [1, 2, 3])
        c.swap(1, 2)
        c_new = Column(3, [1, 3, 2])
        self.assertEqual(c, c_new)

    def test_get_elem(self):
        c = Column(3, [7, -8, 9])
        self.assertEqual(c[1], -8)

    def test_set_elem(self):
        c = Column(3)
        c[2] = 5
        c_new = Column(3, [0, 0, 5])
        self.assertEqual(c, c_new)

    def test_row_minus_row(self):
        c = Column(2, [2, 3])
        c.row_minus_row(0, 1, 1)
        c_new = Column(2, [2, 1])
        self.assertEqual(c, c_new)

    def test_get_size(self):
        c = Column(3)
        self.assertEqual(c.size, (3, 1))


class RowTestCase(unittest.TestCase):
    def test_wrong_size1(self):
        self.assertRaises(DataSizeError, Row, 3, [])

    def test_wrong_size2(self):
        self.assertRaises(DataSizeError, Row, 3, [2, 1])

    def test_str(self):
        r = Row(3)
        self.assertEqual(str(r), "0 0 0")

    def test_repr(self):
        r = Row(2)
        self.assertEqual(r, eval(r.__repr__()))

    def test_equal(self):
        r = Row(2, [2, 4])
        self.assertTrue(r == r)

    def test_non_equal1(self):
        r1 = Row(2, [4, 7])
        r2 = Row(2, [4, 8])
        self.assertFalse(r1 == r2)

    def test_non_equal2(self):
        r1 = Row(2)
        r2 = Row(5)
        self.assertFalse(r1 == r2)

    def test_swap(self):
        r = Row(3, [1, 2, 3])
        r.swap(1, 2)
        r_new = Row(3, [1, 3, 2])
        self.assertEqual(r, r_new)

    def test_get_elem(self):
        r = Row(3, [7, -8, 9])
        self.assertEqual(r[1], -8)

    def test_set_elem(self):
        r = Row(3)
        r[2] = 5
        r_new = Row(3, [0, 0, 5])
        self.assertEqual(r, r_new)

    def test_get_size(self):
        r = Row(5)
        self.assertEqual(r.size, (1, 5))


class MultiplicationTestCase(unittest.TestCase):
    def test_row_on_column(self):
        r = Row(3, [1, 1, 1])
        c = Column(3, [1, 1, 1])
        self.assertEqual(r * c, 3)

    def test_column_on_row(self):
        r = Row(2, [1, 3])
        c = Column(2, [2, 2])
        res = Matrix((2, 2), [[2, 6], [2, 6]])
        self.assertEqual(c * r, res)

    def test_matrix_on_column(self):
        c = Column(3, [3, 2, 1])
        m = Matrix((2, 3), [[1, 7, 4], [-5, 6, 2]])
        res = Column(2, [21, -1])
        self.assertEqual(m * c, res)

    def test_row_on_matrix(self):
        r = Row(2, [2, 3])
        m = Matrix((2, 3), [[4, 2, 1], [3, 5, -4]])
        res = Row(3, [17, 19, -10])
        self.assertEqual(r * m, res)

    def test_2_square(self):
        m1 = Matrix((2, 2), [[1, 7], [5, 12]])
        m2 = Matrix((2, 2), [[3, 9], [-2, -7]])
        res = Matrix((2, 2), [[-11, -40], [-9, -39]])
        self.assertEqual(m1 * m2, res)

    def test_2_non_square(self):
        m1 = Matrix((2, 3), [[1, 2, 3], [5, 2, 9]])
        m2 = Matrix((3, 2), [[1, -3], [7, -5], [10, 2]])
        res = Matrix((2, 2), [[45, -7], [109, -7]])
        self.assertEqual(m1 * m2, res)


if __name__ == '__main__':
    unittest.main()
