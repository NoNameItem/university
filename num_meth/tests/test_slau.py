#!/usr/bin/env python3

import unittest

from slau import Slau
from slau import NotEqualSizeError
from matrices import Matrix
from matrices import Column


class MyTestCase(unittest.TestCase):
    def test_wrong_sizes(self):
        self.assertRaises(NotEqualSizeError, Slau, Matrix((3, 2)), Column(4))

    def test_repr(self):
        slau = Slau(Matrix((3, 2)), Column(3))
        self.assertEqual(slau, eval(slau.__repr__()))

    def test_str(self):
        slau = Slau(Matrix((3, 3), [[1, 1, -7], [-1, 1, 5], [-2, 7, 1]]), Column(3, [5, 3,
                                                                                     4]))
        string = "x1 + x2 - 7x3 = 5\n" \
                 "-x1 + x2 + 5x3 = 3\n" \
                 "-2x1 + 7x2 + x3 = 4"
        self.assertEqual(string, str(slau))

    def test_eq(self):
        s = Slau(Matrix((2, 2), [[1, 3], [5, 2]]), Column(2, [1, 7]))
        self.assertEqual(s, s)

    def test_ext_matrix(self):
        s = Slau(Matrix((2, 2), [[3, 7], [5, 9]]), Column(2, [1, 7]))
        m = Matrix((2, 3), [[3, 7, 1], [5, 9, 7]])
        self.assertEqual(m, s.extended_matrix)

if __name__ == '__main__':
    unittest.main()
