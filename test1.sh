#! /bin/bash

program=./prg1.csh

log=$program.log

if [ ! -f $program ]; then
    echo "Wrong program name"
    exit 1
fi

count=0

echo "Log file" > $log
date >> $log
echo >> $log

# Test #1

echo "Test #1" >> $log
echo "Running $program" >> $log
echo "Expected message: Wrong call. Use $program FROM TO file1 [file2 file3...]" >> $log
echo "Expected exit code: 7" >> $log
msg=`$program`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "Wrong call. Use $program FROM TO file1 [file2 file3...]") && ( $exc -eq 7) ]]; then
    echo "Test #1 passed" >> $log
    let count=count+1
else
    echo "Test #1 failed" >> $log
fi
echo  >> $log

#Test #2

echo "Test #2" >> $log
echo "Running $program Cat_1" >> $log
echo "Expected message: Wrong call. Use $program FROM TO file1 [file2 file3...]" >> $log
echo "Expected exit code: 7" >> $log
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
msg=`$program Cat_1`
exc=$?
if [[ ( "$msg" == "Wrong call. Use $program FROM TO file1 [file2 file3...]") && ( $exc -eq 7) ]]; then
    echo "Test #2 passed" >> $log
    let count=count+1
else
    echo "Test #2 failed" >> $log
fi
echo  >> $log

#Test #3

echo "Test #3" >> $log
echo "Running $program Matr_ica Gurvica" >> $log
echo "Expected message: Wrong call. Use $program FROM TO file1 [file2 file3...]" >> $log
echo "Expected exit code: 7" >> $log
msg=`$program Matr_ica Gurvica`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "Wrong call. Use $program FROM TO file1 [file2 file3...]") && ( $exc -eq 7) ]]; then
    echo "Test #3 passed" >> $log
    let count=count+1
else
    echo "Test #3 failed" >> $log
fi
echo  >> $log

#Test 4

echo "Test #4" >> $log
echo "Running ./$program jerebenok edinirozhka a.txt" >> $log
echo "Expected message: Directory jerebenok not found" >> $log
echo "Expected exit code: 2" >> $log
msg=`$program jerebenok edinorozhka a.txt`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "Directory jerebenok not found") && ( $exc -eq 2) ]]; then
    echo "Test #4 passed" >> $log
    let count=count+1
else
    echo "Test #4 failed" >> $log
fi
echo  >> $log

#Test 5
mkdir pandochka
chmod 000 pandochka

echo "Test #5" >> $log
echo "Running $program pandochka zebrochka trava.txt" >> $log
echo "Expected message: Directory zebrochka not found" >> $log
echo "Expected exit code: 3" >> $log
msg=`$program pandochka zebrochka trava.txt`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "Directory zebrochka not found") && ( $exc -eq 3) ]]; then
    echo "Test #5 passed" >> $log
    let count=count+1
else
    echo "Test #5 failed" >> $log
fi
echo  >> $log

chmod 700 pandochka
rmdir pandochka

#Test #6

mkdir omn

chmod 000 omn

echo "Test #6" >> $log
echo "Running $program omn ./omn om" >> $log
echo "Expected message: Directories omn and ./omn are the same" >> $log
echo "Expected exit code: 1" >> $log
msg=`$program omn ./omn om`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "Directories omn and ./omn are the same") && ( $exc -eq 1) ]]; then
    echo "Test #6 passed" >> $log
    let count=count+1
else
    echo "Test #6 failed" >> $log
fi
echo  >> $log

chmod 700 omn
rmdir omn


#Test #7

mkdir marmelad derevo

chmod 000 marmelad
chmod 000 derevo

echo "Test #7" >> $log
echo "Running $program marmelad derevo kraski.rar" >> $log
echo "Expected message: You don't have enough rights to execute directory marmelad" >> $log
echo "Expected exit code: 4" >> $log
msg=`$program marmelad derevo kraski.rar`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "You don't have enough rights to execute directory marmelad") && ( $exc -eq 4) ]]; then
    echo "Test #7 passed" >> $log
    let count=count+1
else
    echo "Test #7 failed" >> $log
fi
echo  >> $log

chmod 700 marmelad
chmod 700 derevo
rmdir marmelad derevo

#Test #8


mkdir directory not

chmod 100 directory
chmod 000 not

echo "Test #8" >> $log
echo "Running $program directory not found" >> $log
echo "Expected message: You don't have enough rights to execute directory not" >> $log
echo "Expected exit code: 5" >> $log
msg=`$program directory not found`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "You don't have enough rights to execute directory not") && ( $exc -eq 5) ]]; then
    echo "Test #8 passed" >> $log
    let count=count+1
else
    echo "Test #8 failed" >> $log
fi
echo  >> $log

rmdir directory not

#Test #9

mkdir doctor dalek
chmod 100 doctor
chmod 100 dalek

echo "Test #9" >> $log
echo "Running $program doctor dalek EX/ TER/MI/NATE" >> $log
echo "Expected message: Incorrect filename: EX/ TER/MI/NATE
Copy failed, copied 0 file(s)" >> $log
echo "Expected exit code: 6" >> $log
msg=`$program doctor dalek "EX/ TER/MI/NATE"`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "Incorrect filename: EX/ TER/MI/NATE
Copy failed, copied 0 file(s)") && ( $exc -eq 6) ]]; then
    echo "Test #9 passed" >> $log
    let count=count+1
else
    echo "Test #9 failed" >> $log
fi
echo  >> $log

rmdir doctor dalek

#Test #10

mkdir wunderborsch wunderwoman
chmod 100 wunderborsch
chmod 100 wunderwoman

echo "Test #10" >> $log
echo "Running $program wunderborsch wunderwoman cook.txt" >> $log
echo "Expected message: File cook.txt not found in directory wunderborsch
Copy failed, copied 0 file(s)" >> $log
echo "Expected exit code: 6" >> $log
msg=`$program wunderborsch wunderwoman cook.txt`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "File cook.txt not found in directory wunderborsch
Copy failed, copied 0 file(s)") && ( $exc -eq 6) ]]; then
    echo "Test #10 passed" >> $log
    let count=count+1
else
    echo "Test #10 failed" >> $log
fi
echo  >> $log

rmdir wunderborsch wunderwoman

#Test #11

mkdir R2d2 c3po
echo "aaa" >> R2d2/droid_.txt
chmod 100 R2d2
chmod 100 c3po
chmod 000 R2d2/droid_.txt

echo "Test #11" >> $log
echo "Running $program R2d2 c3po droid_.txt" >> $log
echo "Expected message: You don't have enough rights to read file R2d2/droid_.txt
Copy failed, copied 0 file(s)" >> $log
echo "Expected exit code: 6" >> $log
msg=`$program R2d2 c3po droid_.txt`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "You don't have enough rights to read file R2d2/droid_.txt
Copy failed, copied 0 file(s)") && ( $exc -eq 6) ]]; then
    echo "Test #11 passed" >> $log
    let count=count+1
else
    echo "Test #11 failed" >> $log
fi
echo >> $log

chmod 700 R2d2
chmod 700 c3po
chmod 700 R2d2/droid_.txt
rm -rf R2d2 c3po

#Test 12

mkdir wookie cookie
echo "aaa" >> wookie/omnomnom
chmod 100 wookie
chmod 100 cookie
chmod 400 wookie/omnomnom

echo "Test #12" >> $log
echo "Running $program wookie cookie omnomnom" >> $log
echo "Expected message: File cookie/omnomnom not exist, skipping omnomnom
Copy failed, copied 0 file(s)" >> $log
echo "Expected exit code: 6" >> $log
msg=`$program wookie cookie omnomnom`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "File cookie/omnomnom not exist, skipping omnomnom
Copy failed, copied 0 file(s)") && ( $exc -eq 6) ]]; then
    echo "Test #12 passed" >> $log
    let count=count+1
else
    echo "Test #12 failed" >> $log
fi
echo >> $log

chmod 700 wookie
chmod 700 cookie
chmod 700 wookie/omnomnom
rm -rf wookie cookie

#Test #13

mkdir sex drugs
echo "aaa" >> sex/rock.n.roll
echo "" >> drugs/rock.n.roll
chmod 100 sex
chmod 100 drugs
chmod 400 sex/rock.n.roll
chmod 000 drugs/rock.n.roll

echo "Test #13" >> $log
echo "Running $program sex drugs rock.n.roll" >> $log
echo "Expected message: You don't have enough rights to write file drugs/rock.n.roll
Copy failed, copied 0 file(s)" >> $log
echo "Expected exit code: 6" >> $log
msg=`$program sex drugs rock.n.roll`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
if [[ ( "$msg" == "You don't have enough rights to write file drugs/rock.n.roll
Copy failed, copied 0 file(s)") && ( $exc -eq 6) ]]; then
    echo "Test #13 passed" >> $log
    let count=count+1
else
    echo "Test #13 failed" >> $log
fi
echo  >> $log

chmod 700 sex
chmod 700 drugs
chmod 700 sex/rock.n.roll
chmod 700 drugs/rock.n.roll
rm -rf sex drugs


#Test #14

mkdir Folder-1 folder-2
echo "aaa" >> Folder-1/file_to_copy
echo >> folder-2/file_to_copy
chmod 100 Folder-1
chmod 100 folder-2
chmod 400 Folder-1/file_to_copy
chmod 200 folder-2/file_to_copy

echo "Test #14" >> $log
echo "Running $program Folder-1 folder-2 file_to_copy" >> $log
echo "Expected message: Copying file file_to_copy
Copied succesfull, copied 1 file(s)" >> $log
echo "Expected exit code: 0" >> $log
msg=`$program Folder-1 folder-2 file_to_copy`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
chmod 400 folder-2/file_to_copy
if [[ ( "$msg" == "Copying file file_to_copy
Copied succesfull, copied 1 file(s)") && ( $exc -eq 0) && (`cmp Folder-1/file_to_copy folder-2/file_to_copy` -eq 0)]]; then
    echo "Test #14 passed" >> $log
    let count=count+1
else
    echo "Test #14 failed"  >> $log
fi
echo >> $log

chmod 700 Folder-1
chmod 700 folder-2
chmod 700 Folder-1/file_to_copy
chmod 700 folder-2/file_to_copy
rm -rf Folder-1 folder-2 file_to_copy

#Test #15

mkdir good files

echo "first" >> good/first
echo "second" >> good/second
echo "third" >> good/third
echo >> files/first
echo >> files/second
echo >> files/third
chmod 100 files
chmod 100 good
chmod 400 good/first
chmod 400 good/second
chmod 400 good/third
chmod 200 files/first
chmod 200 files/second
chmod 200 files/third

echo "Test #15" >> $log
echo "Running $program good files first second third" >> $log
echo "Expected message: Copying file first
Copying file second
Copying file third
Copied succesfull, copied 3 file(s)" >> $log
echo "Expected exit code: 5" >> $log
msg=`$program good files first second third`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
chmod 400 files/first
chmod 400 files/second
chmod 400 files/third
if [[ ( "$msg" == "Copying file first
Copying file second
Copying file third
Copied succesfull, copied 3 file(s)" ) && ( $exc -eq 0) && (`cmp good/first files/first` -eq 0) && (`cmp good/second files/second` -eq 0) && (`cmp good/third files/third` -eq 0)]]; then
    echo "Test #15 passed" >> $log
    let count=count+1
else
    echo "Test #15 failed" >> $log
fi
echo >> $log
chmod -R 700 files
chmod -R 700 good
rm -rf good files


#Test #16

mkdir In Out

echo "aaa" >> In/file1
echo "bbb" >> In/file2
echo "ccc" >> In/file3
echo "ddd" >> In/file4
echo >> Out/file1
echo >> Out/file2
chmod 100 In
chmod 100 Out
chmod 400 In/file1
chmod 400 In/file2
chmod 400 In/file3
chmod 000 In/file4
chmod 200 Out/file1
chmod 000 Out/file2

echo "Test #16" >> $log
echo "Running $program In Out file/0 file1 file2 file3 file4 file5" >> $log
echo "Expected message: Incorrect filename: file/0
Copying file file1
You don't have enough rights to write file Out/file2
File Out/file3 not exist, skipping file3
You don't have enough rights to read file In/file4
File file5 not found in directory In
Copy failed, copied 1 file(s)" >> $log
echo "Expected exit code: 6" >> $log
msg=`$program In Out file/0 file1 file2 file3 file4 file5`
exc=$?
echo "Script message: $msg" >> $log
echo "Script exit code: $exc" >> $log
chmod 400 Out/file1
if [[ ( "$msg" == "Incorrect filename: file/0
Copying file file1
You don't have enough rights to write file Out/file2
File Out/file3 not exist, skipping file3
You don't have enough rights to read file In/file4
File file5 not found in directory In
Copy failed, copied 1 file(s)") && ( $exc -eq 6) && (`cmp Out/file1 In/file1` -eq 0) ]]; then
    echo "Test #16 passed" >> $log
    let count=count+1
else
    echo "Test #16 failed" >> $log
fi
echo >> $log

chmod -R 700 In
chmod -R 700 Out
rm -rf In Out

echo "Passed $count/16 tests." >> $log
echo "End of log file">> $log
