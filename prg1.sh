#!/bin/bash
#Проверка числа аргументов

if [ $# -lt 3 ] 
then
   echo "Wrong call. Use ./prg1.sh FROM TO file1 [file2 file3...]"
   exit 7
fi

FROM=$1
TO=$2
shift 2
#Files=$@

#Проверка существования каталога FROM
if  [ ! -d "$FROM" ]
then
   echo "Directory $FROM not found"
   exit 2
fi

#Проверка существования каталога TO
if [ ! -d "$TO" ]
then
   echo "Directory $TO not found"
   exit 3
fi

#Проверка различности каталогов FROM и TO
FULL_FROM=`readlink -f "$FROM"` 
FULL_TO=`readlink -f "$TO"`
if [[ "$FULL_FROM" == "$FULL_TO" ]]; then
   echo "Directories $FROM and $TO are the same"
   exit 1
fi

#Проверка прав на каталог FROM
if [ ! -x "$FROM" ]
then
    echo "You don't have enough rights to execute directory $FROM"
    exit 4
fi

#Проверка прав на каталог TO
if [ ! -x "$TO" ]
then 
    echo "You don't have enough rights to execute directory $TO"
    exit 5
fi

#копирование файлов
count=0
flag=1
i=1

IFS=`echo -ne "\b"`
for item in $@; do
   #проверка допустимости имени файла
   if [ `expr index "$item" "/"` -gt 0 ]   
        then 
       echo "Incorrect filename: $item"
       flag=0
   #проверка существования файла /FROM/item
   elif [ ! -f "$FROM/$item" ]
   then
       echo "File $item not found in directory $FROM"
       flag=0
    #Проверка доступа к файлу /FROM/item
   elif [ ! -r "$FROM/$item" ]
   then
       echo "You don't have enough rights to read file $FROM/$item"
       flag=0
   #Проверка существования файла /TO/item
   elif [ ! -f "$TO/$item" ]
   then
       echo "File $TO/$item not exist, skipping $item"
       flag=0
   #Проверка доступа к файлу /TO/item
   elif [ ! -w "$TO/$item" ]
   then
       echo "You don't have enough rights to write file $TO/$item"
       flag=0
   else
       #Копирование
       echo "Copying file $item"
       cp -f "$FROM/$item" "$TO"
       let count=count+1
       fi
   let i=i+1
done
IFS=' '
#Финальное сообщение
if [ $flag -eq 1 ]; then
   echo "Copied succesfull, copied $count file(s)"
   exit 0
else
   echo "Copy failed, copied $count file(s)"
   exit 6
fi

