appendL = foldr (++) []

deepMap :: (a -> b) -> [[a]] -> [[b]]
deepMap f l = map (map f) l



permute' [] = [[]]
permute' l = let 
    listOfListsWith a = 
        map (\e -> a : e) $ 
        permute' $ 
        filter (\x -> x /= a) l
    in appendL $ map listOfListsWith l
    
countChange money coins 
    | money == 0 = 1
    | null coins || money < 0 = 0
    |otherwise =  (countChange (money - head coins) coins) + (countChange money (tail coins))
    