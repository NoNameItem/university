length' :: [a] -> Int
length' [] = 0
length' (x:xs) = 1 + length' xs

sign' a
    | a > 0 = 1
    | a == 0 = 1
    | otherwise = -1

abs' a = a * sign' a

quot' :: (Num a, Ord a) => a -> a-> a
quot' x y
    | a < b = 0
    | otherwise = sign'' x y + quot' (sign' x * (a - b)) y
    where sign'' x y = sign' x * sign' y 
          a = abs' x
          b = abs' y
          
replicate' :: (Num i, Ord i) => i -> a -> [a]
replicate' 0 _ = []
replicate' n x = x : (replicate' (n-1) x)

take' :: (Num i, Ord i) => i -> [a] -> [a]
take' 0 _ = []
take' i [] = []
take' i (x : xs) = x : (take' (i - 1) xs)

reverse'' [] a = a
reverse'' (x:xs) a = reverse'' xs (x : a)

reverse' list = reverse'' list []

repeat' :: a -> [a]
repeat' x = x : repeat' x