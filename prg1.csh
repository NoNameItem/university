#! /bin/csh
#Проверка числа аргументов



if ( $#argv < 3) then
    echo "Wrong call. Use ./prg1.csh FROM TO file1 [file2 file3...]"
    exit 7
endif


set FROM = "$1"
set TO = "$2"
shift
shift


#Проверка существования каталога FROM

if( ! -d "$FROM" ) then
    echo "Directory $FROM not found"
    exit 2
endif

#Проверка существования каталога TO
if ( ! -d "$TO" ) then
	echo "Directory $TO not found"
	exit 3
endif

#Проверка различности каталогов FROM и TO
set FULL_FROM = `readlink -f "$FROM"` 
set FULL_TO = `readlink -f "$TO"`
if ( "$FULL_FROM" == "$FULL_TO" ) then
    echo "Directories $FROM and $TO are the same"
    exit 1
endif

#Проверка прав на каталог FROM
if ( ! -x "$FROM" ) then
    echo "You don't have enough rights to execute directory $FROM"
    exit 4
endif

#Проверка прав на каталог TO
if ( ! -x "$TO" ) then 
    echo "You don't have enough rights to execute directory $TO"
    exit 5
endif

#копирование файлов
set count=0
set flag=1
while ($#argv > 0) 
    set item = "$1"
    shift 
	#проверка допустимости имени файла
    if ( `expr index "$item" "/"` > 0 ) then 
        echo "Incorrect filename: $item"
        set flag=0
    #проверка существования файла /FROM/item
    else if ( ! -f "$FROM/$item" ) then
        echo "File $item not found in directory $FROM"
        set flag=0
    #Проверка доступа к файлу /FROM/item
    else if ( ! -r "$FROM/$item" ) then
        echo "You don't have enough rights to read file $FROM/$item"
        set flag=0
    #Проверка существования файла /TO/item
    else if ( ! -f "$TO/$item" ) then
        echo "File $TO/$item not exist, skipping $item"
        set flag=0
    #Проверка доступа к файлу /To/item
    else if ( ! -w "$TO/$item" ) then
        echo "You don't have enough rights to write file $TO/$item"
        set flag=0
    else
        #Копирование
        echo "Copying file $item"
        cp -f "$FROM/$item" "$TO"
        @ count++
    endif 
end
if ( $flag == 1 ) then
	echo "Copied succesfull, copied $count file(s)"
	exit 0
else
	echo "Copy failed, copied $count file(s)"
	exit 6
endif
